# CSVCoder

This Swift package provides a `CSVDecoder` for decoding CSV data into `Decodable` types.

## Adding the dependency

TODO


## How to decode CSV data

There are two ways to use `CSVDecoder`, depending on whether the input contains a header to define column names.


### CSV data with a header record

CSV input:

```
stringValue,intValue,boolValue
arthur,30,true
zaphod,42,false
```

The `Decodable` type may be created without `CodingKeys` if the property names _exactly_ match the field names in the header:

```
struct MyType: Decodable {
    let stringValue: String
    let intValue: Int
    let boolValue: Bool
}
```

If the property names need to be different, then a `CodingKeys` enum is required, to map between the property name and the field name in the CSV header:

```
struct MyType: Decodable {
    let name: String
    let age: Int
    let isAdmin: Bool

    enum CodingKeys: String, CodingKey {
        case name = "stringValue"
        case age = "intValue"
        case isAdmin = "boolValue"
    }
}
```

Decoding the CSV data is done like this:

```
let decoder = CSVDecoder(expectHeader: true)
let values = try decoder.decode([MyType].self, from: csvString)
```

### CSV data without a header record

CSV input:

```
arthur,30,true
zaphod,42,false
```

If the input CSV has no header record, then a `CodingKeys` enum is required, and it must either extend `Int` or adopt `CaseIterable`. The order of cases in the `CodingKeys` enum _must_ match the order of fields in the input CSV.

Example: `CodingKeys` extends `Int`:

```
struct MyType: Decodable {
    let stringValue: String
    let intValue: Int
    let boolValue: Bool

    enum CodingKeys: Int, CodingKey {
        case stringValue
        case intValue
        case boolValue
    }
}
```

Example: `CodingKeys` extends `String`, so must also adopt `CaseIterable`:

```
struct MyType: Decodable {
    let stringValue: String
    let intValue: Int
    let boolValue: Bool

    enum CodingKeys: String, CodingKey, CaseIterable {
        case stringValue
        case intValue
        case boolValue
    }
}
```

Decoding the CSV data is done like this:

```
let decoder = CSVDecoder(expectHeader: false)
let values = try decoder.decode([MyType].self, from: csvString)
```

## Other usage notes

The decoder may throw errors. See `CSVDecoder.decode(_:,from:)` documentation for details.

The decoder may emit warnings, to `stdout` by default. This can be configured by setting the `warningOutput` property:

```
decoder.warningOutput = .none
```

Valid warning outputs are:

* `.none`: suppresses warning output
* `.stdout`: standard out (the default)
* `.custom(CustomWarningOutput)`: provide a custom type, which adopts the `CustomWarningOutput` protocol, to receive warnings
* `.logger(Logger)`: send warnings to [SwiftLog][SwiftLog], if that package is included in the project


# Supported features

* CSV input with and without the header record
* escaping double-quote (") with another double-quote, when the whole string is enclosed in matching double-quotes
* delimiters and newlines inside double-quoted values
* use of delimiters other than commas


# Limitations

* does not attempt to automatically determine the delimiter used in the input
* does not attempt to automatically determine the presence of a header record
* cannot yet injest data from from other sources (including `InputStream`)
* no DocC


# Correctness

This decoder aims to be compliant with the [CSV Spec][CSVSpec] by [Jim Myhrberg][JimMyhrberg], and the unit tests are derived directly from Jim's spec. (Thank-you Jim!)

If you find a bug, please raise an issue in GitLab. Bonus points for providing a failing unit-test. ;-)


[CSVSpec]: https://csv-spec.org
[JimMyhrberg]: https://jimeh.me/
[SwiftLog]: https://github.com/apple/swift-log
