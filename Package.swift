// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "CSVCoder",
	products: [
		// Products define the executables and libraries a package produces, and make them visible to other packages.
		.library(
			name: "CSVCoder",
			targets: ["CSVCoder"]),
	],
	dependencies: [
		// Dependencies declare other packages that this package depends on.
		.package(url: "http://github.com/mflint/swiftmock", .upToNextMajor(from: "5.0.0")),
	],
	targets: [
		// Targets are the basic building blocks of a package. A target can define a module or a test suite.
		// Targets can depend on other targets in this package, and on products in packages this package depends on.
		.target(
			name: "CSVCoder",
			dependencies: []),
		.testTarget(
			name: "CSVCoderTests",
			dependencies: [
				"CSVCoder",
				.product(name: "SwiftMock", package: "swiftmock"),
			]),
	]
)
