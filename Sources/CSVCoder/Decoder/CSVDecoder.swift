import Foundation

#if canImport(Logging)
	import Logging
#endif

/// A protocol for types that wish to consume parser warnings.
public protocol CustomWarningOutput {
	/// Handle a warning message from the CSV parser.
	/// - Parameter message: The warning message.
	func warning(_ message: String)
}

/// An enum which controls the destination for parser warnings.
public enum WarningOutput {
	/// Parser warnings will be suppressed.
	case none
	/// Parser warnings will be sent to `stdout`.
	case stdout
	/// Parser warnings will be sent to a type which adopts the
	/// ``CustomWarningOutput`` protocol.
	case custom(CustomWarningOutput)
#if canImport(Logging)
	/// Parser warnings will be sent to the provided SwiftLogger.
	case logger(Logger)
#endif

	/// Handle a warning message from the CSV parser.
	/// - Parameter message: The warning message.
	func warning(_ message: String) {
		switch self {
		case .none:
			break
		case .stdout:
			// Not unit-tested
			print("Warning: \(message)")
		case .custom(let output):
			output.warning(message)
#if canImport(Logging)
		case .logger(let logger):
			// Not unit-tested
			logger.warning(message)
#endif
		}
	}
}

private protocol Reader {
	func next() -> Character?
}

private class StringReader: Reader {
	private var iterator: String.Iterator

	init(_ string: String) {
		self.iterator = string.makeIterator()
	}

	func next() -> Character? {
		self.iterator.next()
	}
}

/// An object which decodes instances of a datatype from CSV text.
public class CSVDecoder {
	/// An enumeration of errors that might be thrown by the CSV parser.
	public enum CSVDecodingError: Error {
		/// The provided character may not be used as a delimiter.
		case illegalDelimiter(character: Character)
		/// A header record was expected in the CSV file, but the file was empty.
		case missingHeader
		/// A header record was provided in the CSV file, but a subsequent data
		/// record contained a different number of fields.
		case incorrectFieldCount(headerFieldCount: UInt, fieldCount: UInt, lineNumber: UInt)
		/// The CSV parser found  an opening double-quote character (") in an
		/// unexpected place.
		case unexpectedOpeningQuote(prefix: String, lineNumber: UInt)
		/// The CSV parser unexpectely found some extra non-whitespace
		/// characters after a closing quote-quote character (").
		case illegalCharacterAfterClosingQuote(character: Character, lineNumber: UInt)
		/// The order of the `Decodable`'s `CodingKey enum values could not be
		/// determined. `CodingKey` enums should either be an `Int` type, or adopt
		/// the `CaseIterable` protocol.
		case codingKeysNotIndexable(type: CodingKey.Type)
	}

	private static let illegalDelimiters: [Character] = ["\"", "\n", "\r"]
	private let expectHeader: Bool
	private let delimiter: Character

	/// Controls the destination for warning messages generated by the
	/// CSV parser.
	public var warningOutput: WarningOutput = .stdout

	/// Creates a new decoder using comma as the delimiter.
	/// - Parameter expectHeader: Specifies whether there's a header record in the
	///   CSV input.
	public init(expectHeader: Bool) {
		self.expectHeader = expectHeader
		self.delimiter = ","
	}

	/// Creates a new decoder with an alternate delimiter.
	/// - Parameters:
	///   - expectHeader: Specifies whether there's a header record in the
	///   CSV input.
	///   - delimiter: The delimiter character to use while parsing the input.
	/// - Throws: ``CSVDecodingError/illegalDelimiter(character:)`` if the
	/// supplied delimiter is not permitted.
	public init(expectHeader: Bool, delimiter: Character) throws {
		if Self.illegalDelimiters.contains(delimiter) {
			throw CSVDecodingError.illegalDelimiter(character: delimiter)
		}

		self.expectHeader = expectHeader
		self.delimiter = delimiter
	}

	/// Attempts to decode a CSV string into the type `type`.
	/// - Parameters:
	///   - type: The `Decodable` type to be created.
	///   - string: Input CSV.
	/// - Returns: An array of `Decodable`s, one for each data record in the
	/// input.
	/// - Throws: Various errors if the input is malformed.
	/// - Throws: ``CSVDecodingError.missingHeader`` if a header is required
	/// but the input has no records.
	/// - Throws: ``CSVDecodingError.incorrectFieldCount`` if a header
	/// record is provided, but a subsequent data record has a different number of fields.
	/// - Throws: `DecodingError.keyNotFound` if the input has a header, but
	/// no header key matches a required value in the `Decodable`.
	/// - Throws: `DecodingError.keyNotFound` if the input has no header, but
	/// there are more required values in the `Decodable`'s `CodingKeys` enum than there are fields in a data record.
	/// - throws: `DecodingError.typeMismatch` if a string value from the
	/// input cannot be converted into the.
	public func decode<T: Decodable>(_ type: [T].Type, from string: String) throws -> [T]  {
		var rows: [[String]] = []
		var lineNumber: UInt = 1
		var rowBuilder = RowBuilder(delimiter: self.delimiter,
									warningOutput: self.warningOutput,
									lineNumber: lineNumber)
		let reader = StringReader(string)
		while let next = reader.next() {
			// offer the next character to the RowBuilder
			if let row = try rowBuilder.accept(next) {
				// the RowBuilder returns a complete row, then store the result and make
				// a new RowBuilder
				rows.append(row)

				lineNumber += 1
				rowBuilder = RowBuilder(delimiter: self.delimiter,
										warningOutput: self.warningOutput,
										lineNumber: lineNumber)
			}
		}

		if let finalRow = rowBuilder.finishRow() {
			rows.append(finalRow)
		}

		// if the input CSV has a header record, then we can match fields to
		// CodingKeys using their names
		let headerFieldKeys: [String]?
		let headerLineOffset: UInt
		if self.expectHeader {
			// if we're expecting a header, there must be at least one row
			if rows.isEmpty {
				throw CSVDecodingError.missingHeader
			}
			headerFieldKeys = rows.removeFirst()
			headerLineOffset = 1
		} else {
			headerFieldKeys = nil
			headerLineOffset = 0
		}

		return try rows.enumerated().map { (lineNumber, row) in
			let record: RecordValues

			if let headerFieldKeys {
				// we have a set of field keys from the header record - so check that
				// the number matches the values in this data record
				if headerFieldKeys.count != row.count {
					throw CSVDecodingError.incorrectFieldCount(headerFieldCount: UInt(headerFieldKeys.count),
															   fieldCount: UInt(row.count),
															   lineNumber: UInt(lineNumber + 1) + headerLineOffset)
				}

				// the record is a map from header field keys to field values
				let map = headerFieldKeys.enumerated().reduce(into: [String : String]()) { partialResult, indexAndHeaderFieldKey in
					let (index, headerFieldKey) = indexAndHeaderFieldKey
					partialResult[headerFieldKey] = row[index]
				}
				record = .named(map)
			} else {
				// the record is an array of field values, and we'll rely on the
				// position in this array to find the correct property in our
				// `Decodable`
				record = .ordered(row)
			}

			return try T.init(from: CSVDecoding(record: record, lineNumber: UInt(lineNumber) + headerLineOffset + 1))
		}
	}
}
