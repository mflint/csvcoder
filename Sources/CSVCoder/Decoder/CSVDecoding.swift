import Foundation

private typealias CaseIterableAndEquatable = CaseIterable & Equatable
private typealias CaseIterableCodingKey = CodingKey & CaseIterableAndEquatable

private extension CodingKey where Self: CaseIterable {
	var allCases: Self.AllCases {
		type(of: self).allCases
	}
}

private extension CodingKey where Self: CaseIterableAndEquatable {
	func index() -> Int {
		self.allCases.firstIndex(of: self) as! Int
	}
}

internal struct CSVDecoding: Decoder {
	let codingPath: CodingPath
	let userInfo: UserInfo
	private let record: RecordValues
	private let lineNumber: UInt

	init(codingPath: CodingPath = [], userInfo: UserInfo = UserInfo(), record: RecordValues, lineNumber: UInt) {
		self.codingPath = codingPath
		self.userInfo = userInfo
		self.record = record
		self.lineNumber = lineNumber
	}

	func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey {
		let container = CSVKeyedDecoding<Key>(codingPath: self.codingPath,
											  userInfo: self.userInfo,
											  record: self.record,
											  lineNumber: self.lineNumber)
		return KeyedDecodingContainer(container)
	}

	func unkeyedContainer() throws -> UnkeyedDecodingContainer {
		preconditionFailure()
	}

	func singleValueContainer() throws -> SingleValueDecodingContainer {
		switch self.record {
		case .named, .ordered:
			// not unit tested
			throw DecodingError.keyNotFound(codingPath.last!, DecodingError.Context(codingPath: codingPath, debugDescription: "No single value for \(codingPath)"))
		case .single(let value):
			return SingleValueContainer(codingPath: self.codingPath,
										userInfo: self.userInfo,
										value: value,
										lineNumber: self.lineNumber)
		}
	}
}

private struct CSVKeyedDecoding<Key: CodingKey>: KeyedDecodingContainerProtocol {
	private let record: RecordValues
	private let lineNumber: UInt

	/// The path of coding keys taken to get to this point in decoding.
	let codingPath: CodingPath
	var userInfo: UserInfo

	/// All the keys the `Decoder` has for this container.
	///
	/// Different keyed containers from the same `Decoder` may return different
	/// keys here; it is possible to encode with multiple key types which are
	/// not convertible to one another. This should report all keys present
	/// which are convertible to the requested type.
	let allKeys: [Key] = []

	init(codingPath: CodingPath, userInfo: UserInfo, record: RecordValues, lineNumber: UInt) {
		self.codingPath = codingPath
		self.userInfo = userInfo
		self.record = record
		self.lineNumber = lineNumber
	}

	/// Returns a Boolean value indicating whether the decoder contains a value
	/// associated with the given key.
	///
	/// The value associated with `key` may be a null value as appropriate for
	/// the data format.
	///
	/// - parameter key: The key to search for.
	/// - returns: Whether the `Decoder` has an entry for the given key.
	func contains(_ key: Key) -> Bool {
		switch self.record {
		case .named(let map):
			return map.contains { key.stringValue == $0.key }
		case .ordered(let values):
			if let index = key.intValue {
				return index < values.count
			}
			
			if let orderedKey = key as? any CaseIterableCodingKey {
				let index = orderedKey.index()
				return index < values.count
			}
		case .single:
			return false
		}

		return false
	}

	/// Decodes a null value for the given key.
	///
	/// - parameter key: The key that the decoded value is associated with.
	/// - returns: Whether the encountered value was null.
	/// - throws: `DecodingError.keyNotFound` if `self` does not have an entry
	///   for the given key.
	func decodeNil(forKey key: Self.Key) throws -> Bool {
		let stringValue = try getStringValue(forKey: key)
		return stringValue.isEmpty
	}

	private func getStringValue(forKey codingKey: Key) throws -> String {
		// `self.data` is all the data from a record in the CSV file, expressed as
		// a collection of `String`s. It always has an order (the order of the
		// values in the input record), and _may_ have key names too, if the input
		// file has a header record.

		// we've been given a `key`, which represents the value in our `Decodable`
		// type that we're trying to decode, so we need to find the correct
		// `String` value in the data for this key

		switch self.record {
		case .named(let keyValueMap):
			// the input has a header record, so our data is a key-value map
			guard let stringValue = keyValueMap[codingKey.stringValue] else {
				throw DecodingError.keyNotFound(codingKey, .init(codingPath: self.codingPath, debugDescription: "No header field with name '\(codingKey.stringValue)'"))
			}
			return stringValue
		case .ordered(let stringValues):
			// the input order of the fields is significant, because we have no
			// header record with field names. So we need to find the index of
			// this CodingKey in the CodingKeys enum, so we know which column in
			// the input CSV to get a value from
			let index: Int

			// first, check if the CodingKey has an Int value
			if let unwrappedIndex = codingKey.intValue {
				// it does!
				index = unwrappedIndex
			} else if let key = codingKey as? any CaseIterableCodingKey {
				// it doesn't have an Int value we can use, so the `CodingKey` must
				// be `CaseIterable` (ordered) for us to find the index of this key
				index = key.index()
			} else {
				throw CSVDecoder.CSVDecodingError.codingKeysNotIndexable(type: type(of: codingKey))
			}

			guard index < stringValues.count else {
				throw DecodingError.keyNotFound(codingKey, .init(codingPath: self.codingPath, debugDescription: "No value found at index \(index) on line \(self.lineNumber)"))
			}

			return stringValues[index]
		case .single(let stringValue):
			return stringValue
		}
	}

	/// Decodes a value of the given type for the given key.
	///
	/// - parameter type: The type of value to decode.
	/// - parameter key: The key that the decoded value is associated with.
	/// - returns: A value of the requested type, if present for the given key
	///   and convertible to the requested type.
	/// - throws: `DecodingError.typeMismatch` if the encountered encoded value
	///   is not convertible to the requested type.
	/// - throws: `DecodingError.keyNotFound` if `self` does not have an entry
	///   for the given key.
	/// - throws: `DecodingError.valueNotFound` if `self` has a null entry for
	///   the given key.
	func decode<T>(_ theType: T.Type, forKey key: Key) throws -> T where T: Decodable, T: StringInitializable {
		let stringValue = try self.getStringValue(forKey: key)

		// special-case bool values, because the strings must be lower case
		if T.self == Bool.self,
		   let result = T(stringValue.lowercased()) {
			return result
		}

		// for other StringInitializable types, use the
		if let result = T(stringValue) {
			return result
		}

		let context = DecodingError.Context(codingPath: [key],
											debugDescription: "Cannot convert value '\(stringValue)' to type \(theType)")
		throw DecodingError.typeMismatch(theType, context)
	}

	/// Decodes a value of the given type for the given key.
	///
	/// - parameter type: The type of value to decode.
	/// - parameter key: The key that the decoded value is associated with.
	/// - returns: A value of the requested type, if present for the given key
	///   and convertible to the requested type.
	/// - throws: `DecodingError.typeMismatch` if the encountered encoded value
	///   is not convertible to the requested type.
	/// - throws: `DecodingError.keyNotFound` if `self` does not have an entry
	///   for the given key.
	/// - throws: `DecodingError.valueNotFound` if `self` has a null entry for
	///   the given key.
	func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable {
		let stringValue = try self.getStringValue(forKey: key)
		var newPath = self.codingPath
		newPath.append(key)

		let decoder = CSVDecoding(codingPath: newPath, userInfo: self.userInfo, record: .single(stringValue), lineNumber: self.lineNumber)
		return try T(from: decoder)
	}

	/// Decodes a value of the given type for the given key, if present.
	///
	/// This method returns `nil` if the container does not have a value
	/// associated with `key`, or if the value is null. The difference between
	/// these states can be distinguished with a `contains(_:)` call.
	///
	/// - parameter type: The type of value to decode.
	/// - parameter key: The key that the decoded value is associated with.
	/// - returns: A decoded value of the requested type, or `nil` if the
	///   `Decoder` does not have an entry associated with the given key, or if
	///   the value is a null value.
	/// - throws: `DecodingError.typeMismatch` if the encountered encoded value
	///   is not convertible to the requested type.
	func decodeIfPresent<T>(_ type: T.Type, forKey key: Key) throws -> T? where T: Decodable, T: StringInitializable {
		preconditionFailure()
	}

	/// Decodes a value of the given type for the given key, if present.
	///
	/// This method returns `nil` if the container does not have a value
	/// associated with `key`, or if the value is null. The difference between
	/// these states can be distinguished with a `contains(_:)` call.
	///
	/// - parameter type: The type of value to decode.
	/// - parameter key: The key that the decoded value is associated with.
	/// - returns: A decoded value of the requested type, or `nil` if the
	///   `Decoder` does not have an entry associated with the given key, or if
	///   the value is a null value.
	/// - throws: `DecodingError.typeMismatch` if the encountered encoded value
	///   is not convertible to the requested type.
	func decodeIfPresent<T>(_ type: T.Type, forKey key: Key) throws -> T? where T : Decodable {
		let stringValue = try self.getStringValue(forKey: key)
		if stringValue.isEmpty {
			return nil
		}

		let decoding = CSVDecoding(codingPath: self.codingPath,
								   userInfo: self.userInfo,
								   record: .single(stringValue),
								   lineNumber: self.lineNumber)

		return try T.init(from: decoding)
	}

	/// Returns the data stored for the given key as represented in a container
	/// keyed by the given key type.
	///
	/// - parameter type: The key type to use for the container.
	/// - parameter key: The key that the nested container is associated with.
	/// - returns: A keyed decoding container view into `self`.
	/// - throws: `DecodingError.typeMismatch` if the encountered stored value is
	///   not a keyed container.
	func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
		preconditionFailure()
	}

	/// Returns the data stored for the given key as represented in an unkeyed
	/// container.
	///
	/// - parameter key: The key that the nested container is associated with.
	/// - returns: An unkeyed decoding container view into `self`.
	/// - throws: `DecodingError.typeMismatch` if the encountered stored value is
	///   not an unkeyed container.
	func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer {
		preconditionFailure()
	}

	/// Returns a `Decoder` instance for decoding `super` from the container
	/// associated with the default `super` key.
	///
	/// Equivalent to calling `superDecoder(forKey:)` with
	/// `Key(stringValue: "super", intValue: 0)`.
	///
	/// - returns: A new `Decoder` to pass to `super.init(from:)`.
	/// - throws: `DecodingError.keyNotFound` if `self` does not have an entry
	///   for the default `super` key.
	/// - throws: `DecodingError.valueNotFound` if `self` has a null entry for
	///   the default `super` key.
	func superDecoder() throws -> Decoder {
		preconditionFailure()
	}

	/// Returns a `Decoder` instance for decoding `super` from the container
	/// associated with the given key.
	///
	/// - parameter key: The key to decode `super` for.
	/// - returns: A new `Decoder` to pass to `super.init(from:)`.
	/// - throws: `DecodingError.keyNotFound` if `self` does not have an entry
	///   for the given key.
	/// - throws: `DecodingError.valueNotFound` if `self` has a null entry for
	///   the given key.
	func superDecoder(forKey key: Key) throws -> Decoder {
		preconditionFailure()
	}
}

private struct SingleValueContainer: SingleValueDecodingContainer {
	let codingPath: CodingPath
	private let userInfo: UserInfo
	private let value: String
	private let lineNumber: UInt

	init(codingPath: CodingPath, userInfo: UserInfo, value: String, lineNumber: UInt) {
		self.codingPath = codingPath
		self.userInfo = userInfo
		self.value = value
		self.lineNumber = lineNumber
	}

	func decodeNil() -> Bool {
		self.value.isEmpty
	}

	func decode<T>(_ type: T.Type) throws -> T where T: Decodable, T: StringInitializable {
		guard let value = T(self.value) else {
			let context = DecodingError.Context(codingPath: self.codingPath,
												debugDescription: "Cannot convert value '\(self.value)' to type \(type)")
			throw DecodingError.typeMismatch(type, context)
		}

		return value
	}

	func decode<T>(_ type: T.Type) throws -> T where T: Decodable {
		let decoder = CSVDecoding(codingPath: self.codingPath,
								  userInfo: self.userInfo,
								  record: .single(self.value),
								  lineNumber: self.lineNumber)
		return try T(from: decoder)
	}
}

private protocol StringInitializable {
	init?(_ source: String)
}

extension Int: StringInitializable {}
extension Int8: StringInitializable {}
extension Int16: StringInitializable {}
extension Int32: StringInitializable {}
extension Int64: StringInitializable {}
extension UInt: StringInitializable {}
extension UInt8: StringInitializable {}
extension UInt16: StringInitializable {}
extension UInt32: StringInitializable {}
extension UInt64: StringInitializable {}
extension Double: StringInitializable {}
extension Float: StringInitializable {}
extension Bool: StringInitializable {}
extension String: StringInitializable {}
