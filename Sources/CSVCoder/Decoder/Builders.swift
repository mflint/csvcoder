import Foundation

internal struct RowBuilder {
	private let delimiter: Character
	private let warningOutput: WarningOutput
	private let lineNumber: UInt

	private var fieldBuilder: FieldBuilder?
	private var fields: [String] = []

	init(delimiter: Character, warningOutput: WarningOutput, lineNumber: UInt) {
		self.delimiter = delimiter
		self.warningOutput = warningOutput
		self.lineNumber = lineNumber
	}

	mutating func accept(_ character: Character) throws -> [String]? {
		if self.fieldBuilder == nil {
			self.fieldBuilder = FieldBuilder(delimiter: self.delimiter,
											 warningOutput: self.warningOutput,
											 lineNumber: self.lineNumber)
		}

		if let field = try fieldBuilder?.accept(character) {
			// fieldbuilder is finished, so store the complete field
			fields.append(field)

			self.fieldBuilder = FieldBuilder(delimiter: self.delimiter,
											 warningOutput: self.warningOutput,
											 lineNumber: self.lineNumber)

			// if the field was finished by a new line, then the row is also finished
			if character.isNewline {
				return self.fields
			}
		}

		// row is not finished
		return nil
	}

	mutating func finishRow() -> [String]? {
		if let incompleteField = self.fieldBuilder?.string {
			self.fields.append(incompleteField)
			self.fieldBuilder = nil
			return self.fields
		}

		return nil
	}
}

internal struct FieldBuilder {
//	private static let whitespaceExcludingNewlines = CharacterSet(charactersIn: " \t")

	enum DoubleQuoteState {
		/// No double quote has been detected while building this field.
		case none
		/// A first double quote has been detected.
		case inside
		/// A second double quote has been detected - but we don't know if this
		/// double quote is escaping another double quote, or it's marking the end
		/// of the quoted field. We need to see the next character, which should
		/// be either a delimiter, a double quote or whitespace.
		/// The only legal next characters in this state are delimiters, newlines,
		/// double-quote and whitespace. (Whitespace emits a warning)
		case maybeClosingQuote
	}

	private let delimiter: Character
	private let warningOutput: WarningOutput
	private let lineNumber: UInt

	fileprivate var string: String = ""
	private var ignoredTrailingWhitespace = false
	private var quoteState: DoubleQuoteState = .none

	init(delimiter: Character, warningOutput: WarningOutput, lineNumber: UInt) {
		self.delimiter = delimiter
		self.warningOutput = warningOutput
		self.lineNumber = lineNumber
	}

	mutating func accept(_ character: Character) throws -> String? {
		switch self.quoteState {
		case .none:
			// check delimiter first, because it could be changed to something else (tab, for example)
			if character == self.delimiter || character.isNewline {
				return string
			}

			if character == "\"" {
				if !self.string.isEmpty {
					if self.string.trimmingCharacters(in: .whitespaces).isEmpty {
						self.warningOutput.warning("Warning: ignoring unexpected leading space before quoted value on line \(self.lineNumber)")
					} else {
						throw CSVDecoder.CSVDecodingError.unexpectedOpeningQuote(prefix: self.string, lineNumber: self.lineNumber)
					}
				}
				self.string.removeAll()
				self.quoteState = .inside
				return nil
			}

			self.string.append(character)
			return nil
		case .inside:
			if character == "\"" {
				self.quoteState = .maybeClosingQuote
			} else {
				self.string.append(character)
			}
			return nil
		case .maybeClosingQuote:
			// check delimiter first, because it could be changed to something else (tab, for example)
			if character == self.delimiter || character.isNewline {
				return string
			}

			if character == "\"" {
				// we've just seen a double quote which _might_ have been a closing
				// quote... but here comes another one, so it's actually a single
				// escaped double-quote
				self.string.append(character)
				self.quoteState = .inside
				return nil
			}

			if character.isWhitespace {
				if !self.ignoredTrailingWhitespace {
					self.ignoredTrailingWhitespace = true
					self.warningOutput.warning("Warning: ignoring unexpected trailing space after quoted value on line \(self.lineNumber)")
				}
				return nil
			}

			throw CSVDecoder.CSVDecodingError.illegalCharacterAfterClosingQuote(character: character, lineNumber: lineNumber)
		}
	}
}
