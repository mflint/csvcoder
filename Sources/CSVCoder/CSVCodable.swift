import Foundation

public typealias UserInfo = [CodingUserInfoKey: Any]

enum RecordValues {
	/// If the input CSV has a header, then each record is represented as a
	/// map of header field keys to field values
	case named([String : String])
	/// If the input CSV has no header, then each record is a represented as
	/// an array of `String` values.
	case ordered([String])
	case single(String)
}

typealias CodingPath = [CodingKey]
