import XCTest
import SwiftMock
@testable import CSVCoder

private class MockWarningOutput: Mock<CustomWarningOutput>, CustomWarningOutput {
	func warning(_ message: String) {
		accept(func: "functionName", args: [message])
	}
}

/// Many of these testcases are based on the [CSV Spec](https://csv-spec.org)
/// by [Jim Myhrberg](https://jimeh.me/).
final class CSVDecoderTests: XCTestCase {
	/// This has a `CodingKeys` enum which is `CaseIterable`.
	///
	/// If the CSV input has no header, then `CodingKeys` must be supplied
	/// so that the decoder can map input fields to properties in this
	/// `Decodable`. The `CodingKeys` enum _must_ adopt `CaseIterable`.
	private struct ExampleWithOrderedCodingKeys: Decodable, Equatable {
		let theString: String
		let theInt: Int
		let theDouble: Double
		let theBool: Bool

		enum CodingKeys: Int, CodingKey {
			case theString
			case theInt
			case theDouble
			case theBool
		}
	}

	/// This has a `CodingKeys` enum which is `CaseIterable`.
	///
	/// If the CSV input has no header, then `CodingKeys` must be supplied
	/// so that the decoder can map input fields to properties in this
	/// `Decodable`. The `CodingKeys` enum _must_ adopt `CaseIterable`.
	private struct ExampleWithCaseIterableCodingKeys: Decodable, Equatable {
		let theString: String
		let theInt: Int
		let theDouble: Double
		let theBool: Bool

		enum CodingKeys: String, CodingKey, CaseIterable {
			case theString
			case theInt
			case theDouble
			case theBool
		}
	}

	/// This `Decodable` has no `CodingKeys` enum.
	///
	/// If the CSV input has a header record, then `CodingKeys` enum is not
	/// required, because an _implicit_ `CodingKeys` is generated automatically.
	///
	/// The property names in the `Decodable` match the field names in the
	/// CSV header record exactly.
	///
	/// If the names do not match, then a `CodingKeys` enum may be used to
	/// map propert names to CSV field names.
	///
	/// - Note: A _case-insensitive_ comparison is used for matching header
	/// keys with properties.
	private struct ExampleWithoutCodingKeys: Decodable, Equatable {
		let string: String
		let int: Int
		let double: Double
		let bool: Bool
	}

	private let mockWarningOutput = MockWarningOutput.create()

	private func verifyAll(file: StaticString = #file,
						line: UInt = #line) {
		mockWarningOutput.verify(file: file, line: line)
	}

	private func doMakeDecoder(expectHeader: Bool) -> CSVDecoder {
		// when
		let decoder = CSVDecoder(expectHeader: expectHeader)

		// then
		verifyAll()

		// when
		decoder.warningOutput = .custom(self.mockWarningOutput)

		// then
		verifyAll()

		return decoder
	}

	private func doMakeDecoder(expectHeader: Bool, delimiter: Character) throws -> CSVDecoder {
		// when
		let decoder = try CSVDecoder(expectHeader: expectHeader, delimiter: delimiter)

		// then
		verifyAll()

		// when
		decoder.warningOutput = .custom(self.mockWarningOutput)

		// then
		verifyAll()

		return decoder
	}

	// MARK: - "CSV Spec" testcases

	/**
	 Linux, macOS, and other \*NIX operating systems generally use a line feed (LF or \n) character.

	 Each record starts at the beginning of its own line, and ends with a line break (shown as ¬).

	 Line breaks in CSV files can be CRLF (\r\n), LF (\n), and even in rare cases CR (\r).

	 ```
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz¬
	 ```
	 */
	func testNewlineLF() throws {
		// given
		let csv = "s1,1,1.1,true\ns2,2,2.2,false\n"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Windows uses a carriage return (CR or \r) and a line feed character, effectively “CRLF” (\r\n)

	 Each record starts at the beginning of its own line, and ends with a line break (shown as ¬).

	 Line breaks in CSV files can be CRLF (\r\n), LF (\n), and even in rare cases CR (\r).

	 ```
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz¬
	 ```
	 */
	func testNewlineCRLF() throws {
		// given
		let csv = "s1,1,1.1,true\r\ns2,2,2.2,false\r\n"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 CR line break (\r).

	 Each record starts at the beginning of its own line, and ends with a line break (shown as ¬).

	 Line breaks in CSV files can be CRLF (\r\n), LF (\n), and even in rare cases CR (\r).

	 ```
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz¬
	 ```
	 */
	func testNewlineCR() throws {
		// given
		let csv = "s1,1,1.1,true\rs2,2,2.2,false\r"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Though it is RECOMMENDED, the last record in a file is not required to have a ending line break.

	 ```
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz
	 ```
	 */
	func testNewlineLF_lastRecordWithoutLineBreak() throws {
		// given
		let csv = "s1,1,1.1,true\ns2,2,2.2,false"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Optional header line.

	 There may be an OPTIONAL header line appearing as the first line of the file with the same format as normal records. This header will contain names corresponding to the fields in the file, and MUST contain the same number of fields as the records in the rest of the file.

	 ```
	 field_1,field_2,field_3¬
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz¬
	 ```
	 */
	func testOptionalHeaderLine() throws {
		// given
		let csv = "string,int,double,bool\ns1,1,1.1,true\ns2,2,2.2,false\n"
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([ExampleWithoutCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithoutCodingKeys(string: "s1", int: 1, double: 1.1, bool: true),
			ExampleWithoutCodingKeys(string: "s2", int: 2, double: 2.2, bool: false)
		])
		verifyAll()
	}

	/**
	 Optional header line with incorrect number of fields.

	 There may be an OPTIONAL header line appearing as the first line of the file with the same format as normal records. This header will contain names corresponding to the fields in the file, and MUST contain the same number of fields as the records in the rest of the file.

	 ```
	 field_1,field_2¬
	 aaa,bbb,ccc¬
	 xxx,yyy,zzz¬
	 ```
	 */
	func testOptionalHeaderLineWithIncorrectNumberOfFields() throws {
		// given
		let csv = "string,int,double,bool,hummus\ns1,1,1.1,true\ns2,2,2.2,false\n"
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([ExampleWithoutCodingKeys].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.incorrectFieldCount")
		} catch CSVDecoder.CSVDecodingError.incorrectFieldCount(let headerFieldCount, let fieldCount, let line) {
			XCTAssertEqual(headerFieldCount, 5)
			XCTAssertEqual(fieldCount, 4)
			XCTAssertEqual(line, 2)
		}
		verifyAll()
	}

	/**
	 Illegal trailing separator.

	 The last field in a record MUST NOT be followed by a comma. This results in a additional field with nothing in it.

	 ```
	 aaa,bbb,ccc,¬
	 xxx,yyy,zzz,¬
	 ```

	 - Note: This test is only valid when the input CSV has a header record.
	 When parsing _ordered_ fields, we can't determine if there are too many
	 fields in a record, because the `Decodable` type might be choosing to
	 decode fewer properties than there are fields in the record.
	 */
	func testIllegalTrailingSeparator() throws {
		// given
		let csv = "string,int,double,bool\ns1,1,1.1,true,\ns2,2,2.2,false,\n"
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([ExampleWithoutCodingKeys].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.incorrectFieldCount")
		} catch CSVDecoder.CSVDecodingError.incorrectFieldCount(let headerFieldCount, let fieldCount, let line) {
			XCTAssertEqual(headerFieldCount, 4)
			XCTAssertEqual(fieldCount, 5)
			XCTAssertEqual(line, 2)
		}
		verifyAll()
	}

	/**
	 Spaces are considered part of a field and MUST NOT be ignored.

	 ```
	 aaa ,  bbb , ccc¬
	 xxx, yyy  ,zzz ¬
	 ```
	 */
	func testSpacesInStringField() throws {
		let csv = " s1,1,1.1,true\ns2 ,2,2.2,false\n"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: " s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2 ", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Fields containing line breaks. (LF)

	 Fields containing line breaks (CRLF, LF, or CR), double quotes, or the delimiter character (normally a comma) MUST be enclosed in double-quotes.

	 ```
	 aaa,"b¬
	 bb",ccc¬
	 xxx,"y, yy",zzz¬
	 ```
	 */
	func testFieldsContainingLineBreaks_LF() throws {
		let csv = """
		"s1\ns1",1,1.1,true
		"s2\ns2",2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1\ns1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2\ns2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Fields containing line breaks. (CRLF)

	 Fields containing line breaks (CRLF, LF, or CR), double quotes, or the delimiter character (normally a comma) MUST be enclosed in double-quotes.

	 ```
	 aaa,"b¬
	 bb",ccc¬
	 xxx,"y, yy",zzz¬
	 ```
	 */
	func testFieldsContainingLineBreaks_CRLF() throws {
		let csv = """
		"s1\r\ns1",1,1.1,true
		"s2\r\ns2",2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1\r\ns1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2\r\ns2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Fields containing line breaks. (CR)

	 Fields containing line breaks (CRLF, LF, or CR), double quotes, or the delimiter character (normally a comma) MUST be enclosed in double-quotes.

	 ```
	 aaa,"b¬
	 bb",ccc¬
	 xxx,"y, yy",zzz¬
	 ```
	 */
	func testFieldsContainingLineBreaks_CR() throws {
		let csv = """
		"s1\rs1",1,1.1,true
		"s2\rs2",2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1\rs1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2\rs2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Fields containing a quoted delimiter.

	 Fields containing line breaks (CRLF, LF, or CR), double quotes, or the delimiter character (normally a comma) MUST be enclosed in double-quotes.

	 ```
	 aaa,"b¬
	 bb",ccc¬
	 xxx,"y, yy",zzz¬
	 ```
	 */
	func testFieldsContainingDelimiter() throws {
		let csv = """
		"s1,s1",1,1.1,true
		"s2,s2",2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1,s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2,s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Fields containing double quote.

	 A double-quote appearing inside a field MUST be escaped by preceding it with another double quote, and the field itself MUST be enclosed in double quotes.

	 ```
	 aaa,"b""bb",ccc¬
	 ```
	 */
	func testDoubleQuote() throws {
		// given
		let csv = """
		"s1""s1",1,1.1,true
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1\"s1", theInt: 1, theDouble: 1.1, theBool: true)
		])
		verifyAll()
	}

	/**
	 Illegal space before double quote.

	 When a field enclosed in double quotes has spaces before and/or after the double quotes, the spaces MUST be ignored, as the field starts and ends with the double quotes. However this is considered invalid formatting and the CSV parser SHOULD report some form of warning message.

	 ```
	 aaa,bbb,ccc¬
	 xxx,  "y, yy" ,zzz¬
	 ```
	 */
	func testSpacesBeforeDoubleQuote() throws {
		// given
		let csv = """
		  "s1",1,1.1,true
		s2,2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// expect
		mockWarningOutput.expect { $0.warning("Warning: ignoring unexpected leading space before quoted value on line 1") }

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Illegal space after double quote.

	 When a field enclosed in double quotes has spaces before and/or after the double quotes, the spaces MUST be ignored, as the field starts and ends with the double quotes. However this is considered invalid formatting and the CSV parser SHOULD report some form of warning message.

	 ```
	 aaa,bbb,ccc¬
	 xxx,  "y, yy" ,zzz¬
	 ```
	 */
	func testSpacesAfterDoubleQuote() throws {
		// given
		let csv = """
		s1,1,1.1,true
		"s2"  ,2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// expect
		mockWarningOutput.expect { $0.warning("Warning: ignoring unexpected trailing space after quoted value on line 2") }

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 Every field in double quotes - even values which aren't strings.

	 It is possible to enclose every field in double quotes even if they don’t need to be enclosed. However it is RECOMMENDED to only enclose fields in double quotes that requires it.

	 ```
	 "aaa","bbb","ccc"¬
	 "xxx",yyy,zzz¬
	 ```
	 */
	func testEveryFieldInDoubleQuotes() throws {
		// given
		let csv = """
		"s1","1","1.1","true"
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true)
		])
		verifyAll()
	}

	// All fields are always strings. CSV itself does not support type casting to integers, floats, booleans, or anything else. It is not a CSV library’s responsibility to type cast input CSV data.
	// [Matthew - 20221109] We'll ignore this requirement ;-)


	// MARK: - Other test cases, not derived from Jim's "CSV Spec"

	/**
	 Opening double quote is not permitted if it comes after some non-whitespace characters.
	 */
	func testUnexpectedOpeningDoubleQuote() throws {
		// given
		let csv = """
		zaphod"s1",1,1.1,true
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		do {
			_ = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.unexpectedOpeningQuote")
		} catch CSVDecoder.CSVDecodingError.unexpectedOpeningQuote(let prefix, let line) {
			XCTAssertEqual(prefix, "zaphod")
			XCTAssertEqual(line, 1)
		}
		verifyAll()
	}

	/**
	 When we have a double quote which _potentially_ closes a quoted value, then
	 the only legal next characters are delimiters, newlines, double-quote and
	 whitespace. Other characters should cause an error.
	 */
	func testIllegalCharacterAfterClosingDoubleQuote() throws {
		// given
		let csv = """
		"s1"x,1,1.1,true
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		do {
			_ = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.illegalCharacterAfterClosingQuote")
		} catch CSVDecoder.CSVDecodingError.illegalCharacterAfterClosingQuote(let character, let line) {
			XCTAssertEqual(character, "x")
			XCTAssertEqual(line, 1)
		}
		verifyAll()
	}

	func testNonLowerCaseBoolValues() throws {
		// given
		let csv = "s1,1,1.1,TRUE\ns2,2,2.2,False\n"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/**
	 If a `String` property is non-optional, null values in the CSV translate to empty-string.
	 */
	func testEmptyValueToString() throws {
		// given
		struct Example: Decodable, Equatable {
			let name: String
		}
		let csv = """
		name
		""
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(name: "")
		])
		verifyAll()
	}

	/**
	 If a `String` property is optional, null values in the CSV translate to nil.
	 */
	func testEmptyValueToOptionalString() throws {
		// given
		struct Example: Decodable, Equatable {
			let name: String?
		}
		let csv = """
		name
		""
		matthew
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(name: nil),
			Example(name: "matthew")
		])
		verifyAll()
	}

	func testOtherOptionalTypes() throws {
		// given
		struct Example: Decodable, Equatable {
			let optionalString: String?
			let optionalInt: Int?
			let optionalBool: Bool?
		}
		let csv = """
		optionalString,optionalInt,optionalBool
		"",42,True
		hello,,FALSE
		hello,"12",""
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(optionalString: nil, optionalInt: 42, optionalBool: true),
			Example(optionalString: "hello", optionalInt: nil, optionalBool: false),
			Example(optionalString: "hello", optionalInt: 12, optionalBool: nil)
		])
		verifyAll()
	}

	/**
	 If we cannot convert from a field's `String` value to the expected type
	 in our `Decodable`, then an error will be thrown.
	 */
	func testStringConversionFailureIntoNonOptionalType() throws {
		// given
		struct Example: Decodable, Equatable {
			let value: Int
		}
		let csv = """
		value
		zaphod
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: DecodingError.typeMismatch")
		} catch DecodingError.typeMismatch(let type, let context) {
			XCTAssertTrue(type is Int.Type)
			let codingPath = context.codingPath.map { $0.stringValue }
			XCTAssertEqual(codingPath, ["value"])
			XCTAssertEqual(context.debugDescription, "Cannot convert value 'zaphod' to type Int")
		}
		verifyAll()
	}

	/**
	 If we cannot convert from a field's `String` value to the expected type
	 in our `Decodable`, then an error will be thrown.
	 */
	func testStringConversionFailureIntoOptionalType() throws {
		// given
		struct Example: Decodable, Equatable {
			let value: Double?
		}
		let csv = """
		value
		zaphod
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: DecodingError.typeMismatch(")
		} catch DecodingError.typeMismatch(let type, let context) {
			XCTAssertTrue(type is Double.Type)
			let codingPath = context.codingPath.map { $0.stringValue }
			XCTAssertEqual(codingPath, ["value"])
			XCTAssertEqual(context.debugDescription, "Cannot convert value 'zaphod' to type Double")
		}
		verifyAll()
	}

	func testTabDelimiter() throws {
		// given
		let csv = """
		string	int	double	bool
		s1	1	1.1	true
		s2	2	2.2	false
		"""
		let decoder = try self.doMakeDecoder(expectHeader: true, delimiter: "\t")

		// when
		let result = try decoder.decode([ExampleWithoutCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithoutCodingKeys(string: "s1", int: 1, double: 1.1, bool: true),
			ExampleWithoutCodingKeys(string: "s2", int: 2, double: 2.2, bool: false)
		])
		verifyAll()
	}

	private func doTestIllegalDelimiter(_ delimiter: Character) throws {
		do {
			_ = try CSVDecoder(expectHeader: false, delimiter: delimiter)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.illegalDelimiter")
		} catch CSVDecoder.CSVDecodingError.illegalDelimiter(let illegalDelimiter) {
			XCTAssertEqual(delimiter, illegalDelimiter)
		}
	}

	func testIllegalDelimiters() throws {
		try self.doTestIllegalDelimiter("\"")
		try self.doTestIllegalDelimiter("\n")
		try self.doTestIllegalDelimiter("\r")
	}

	func testNoWarning() throws {
		// given
		let csv = """
		  "s1",1,1.1,true
		s2,2,2.2,false
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)
		decoder.warningOutput = .none

		// expect
		// nothing

		// when
		let result = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithOrderedCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithOrderedCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	func testNoInput() throws {
		// given
		let csv = ""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithoutCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [])
		verifyAll()
	}

	func testNoHeader() throws {
		// given
		let csv = ""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([ExampleWithOrderedCodingKeys].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.missingHeader")
		} catch CSVDecoder.CSVDecodingError.missingHeader {}
		verifyAll()
	}

	func testHeaderNameMismatch() throws {
		// given
		struct Example: Decodable, Equatable {
			let value: String
		}
		let csv = """
		name
		zaphod
		"""
		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: CSVDecoder.noHeaderFieldWithName.missingHeader")
		} catch DecodingError.keyNotFound(let codingKey, let context) {
			XCTAssertEqual(codingKey.stringValue, "value")
			let codingPath = context.codingPath.map { $0.stringValue }
			XCTAssertEqual(codingPath, [])
			XCTAssertEqual(context.debugDescription, "No header field with name 'value'")
		}
		verifyAll()
	}

	func testNoHeaderNoCompatibleCodingKey() throws {
		// given
		struct Example: Decodable, Equatable {
			let value: String

			enum CodingKeys: String, CodingKey {
				case value
			}
		}
		let csv = "zaphod"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.codingKeysNotIndexable")
		} catch CSVDecoder.CSVDecodingError.codingKeysNotIndexable(let type) {
			XCTAssertTrue(type is Example.CodingKeys.Type)
		}
		verifyAll()
	}

	func testCaseIterableStringCodingKeysWithNoInputHeader() throws {
		// given
		let csv = "s1,1,1.1,true\ns2,2,2.2,false\n"
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		let result = try decoder.decode([ExampleWithCaseIterableCodingKeys].self, from: csv)

		// then
		XCTAssertEqual(result, [
			ExampleWithCaseIterableCodingKeys(theString: "s1", theInt: 1, theDouble: 1.1, theBool: true),
			ExampleWithCaseIterableCodingKeys(theString: "s2", theInt: 2, theDouble: 2.2, theBool: false)
		])
		verifyAll()
	}

	/// `Decodable` type expects two fields, but the input data only has one
	func testTooFewFieldInData() throws {
		// given
		struct Example: Decodable, Equatable {
			let firstName: String
			let lastName: String

			enum CodingKeys: String, CodingKey, CaseIterable {
				case firstName
				case lastName
			}
		}
		let csv = """
		zaphod,beeblebrox
		arthur,dent
		trillian
		"""
		let decoder = self.doMakeDecoder(expectHeader: false)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: CSVDecoder.CSVDecodingError.fieldIndexOutOfBounds")
		} catch DecodingError.keyNotFound(let codingKey, let context) {
			XCTAssertEqual(codingKey.stringValue, "lastName")
			let codingPath = context.codingPath.map { $0.stringValue }
			XCTAssertEqual(codingPath, [])
			XCTAssertEqual(context.debugDescription, "No value found at index 1 on line 3")
		}
		verifyAll()
	}

	func testDecodeToCustomTypeStringInitializable() throws {
		// given
		enum Species: String, Decodable {
			case human
			case alien
		}

		struct Example: Decodable, Equatable {
			let name: String
			let species: Species

			enum CodingKeys: String, CodingKey {
				case name
				case species
			}
		}

		let csv = """
		name,species
		zaphod,alien
		arthur,human
		trillian,human
		"""

		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(name: "zaphod", species: .alien),
			Example(name: "arthur", species: .human),
			Example(name: "trillian", species: .human)
		])
		verifyAll()
	}

	func testDecodeToOptionalCustomType() throws {
		// given
		enum Species: String, Decodable {
			case human
			case alien
		}

		struct Example: Decodable, Equatable {
			let name: String
			let species: Species?

			enum CodingKeys: String, CodingKey {
				case name
				case species
			}
		}

		let csv = """
		name,species
		zaphod,alien
		arthur,human
		petunias,
		"""

		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(name: "zaphod", species: .alien),
			Example(name: "arthur", species: .human),
			Example(name: "petunias", species: nil)
		])
		verifyAll()
	}

	/// Decoding a custom enum, but the `Enum.init(String)` will fail because
	/// the enum case in the input doesn't exist in the Enum
	func testDecodeToCustomTypeStringInitializableButDecodingFails() throws {
		// given
		enum Species: String, Decodable {
			case human
			case alien
		}

		struct Example: Decodable, Equatable {
			let name: String
			let species: Species

			enum CodingKeys: String, CodingKey {
				case name
				case species
			}
		}

		let csv = """
		name,species
		zaphod,alien
		arthur,human
		triffid,plant
		"""

		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		do {
			_ = try decoder.decode([Example].self, from: csv)
			XCTFail("expected error: DecodingError.dataCorrupted")
		} catch DecodingError.dataCorrupted {}
		verifyAll()
	}

	func testSingleValueDecoderDecodeNil() throws {
		// given
		enum AlienType: String, Decodable {
			case none
			case betelgeusian

			init(from decoder: Decoder) throws {
				let singleValueContainer = try decoder.singleValueContainer()
				if singleValueContainer.decodeNil() {
					self = .none
					return
				}

				let singleValue = try singleValueContainer.decode(String.self)
				self = .init(rawValue: singleValue)!
			}
		}

		struct Example: Decodable, Equatable {
			let name: String
			let alienType: AlienType

			enum CodingKeys: String, CodingKey {
				case name
				case alienType
			}
		}

		let csv = """
		name,alienType
		zaphod,betelgeusian
		arthur,
		"""

		let decoder = self.doMakeDecoder(expectHeader: true)

		// when
		let result = try decoder.decode([Example].self, from: csv)

		// then
		XCTAssertEqual(result, [
			Example(name: "zaphod", alienType: .betelgeusian),
			Example(name: "arthur", alienType: .none)
		])
		verifyAll()
	}
}
